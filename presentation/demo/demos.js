var demo1_commands = [
    'clear',
];
var demo1_tree = 
{
  "branches": {
    "master": {
      "target": "C2",
      "id": "master",
      "remoteTrackingBranchID": null,
      "remote": false,
      "type": "branch"
    },
    "test": {
      "target": "C4",
      "id": "test",
      "remoteTrackingBranchID": null,
      "remote": false,
      "type": "branch"
    },
    "test2": {
      "target": "C5",
      "id": "test2",
      "remoteTrackingBranchID": null,
      "remote": false,
      "type": "branch"
    }
  },
  "commits": {
    "C0": {
      "parents": [],
      "id": "C0",
      "rootCommit": true,
      "type": "commit",
      "author": "Paulo Romeira",
      "createTime": "Tue May 30 2017 22:58:19 GMT-0300 (BRT)",
      "commitMessage": "Commit rápido"
    },
    "C1": {
      "parents": [
        "C0"
      ],
      "id": "C1",
      "type": "commit",
      "author": "Paulo Romeira",
      "createTime": "Tue May 30 2017 22:58:19 GMT-0300 (BRT)",
      "commitMessage": "Commit rápido"
    },
    "C2": {
      "parents": [
        "C1"
      ],
      "id": "C2",
      "type": "commit",
      "author": "Paulo Romeira",
      "createTime": "Tue May 30 2017 23:06:18 GMT-0300 (BRT)",
      "commitMessage": "Commit rápido"
    },
    "C3": {
      "parents": [
        "C0"
      ],
      "id": "C3",
      "type": "commit",
      "author": "Paulo Romeira",
      "createTime": "Tue May 30 2017 23:06:51 GMT-0300 (BRT)",
      "commitMessage": "Commit rápido"
    },
    "C4": {
      "parents": [
        "C3"
      ],
      "id": "C4",
      "type": "commit",
      "author": "Paulo Romeira",
      "createTime": "Tue May 30 2017 23:06:54 GMT-0300 (BRT)",
      "commitMessage": "Commit rápido"
    },
    "C5": {
      "parents": [
        "C3"
      ],
      "id": "C5",
      "type": "commit",
      "author": "Paulo Romeira",
      "createTime": "Tue May 30 2017 23:07:21 GMT-0300 (BRT)",
      "commitMessage": "Commit rápido"
    }
  },
  "tags": {},
  "HEAD": {
    "id": "HEAD",
    "target": "test2",
    "type": "general ref"
  }
};
